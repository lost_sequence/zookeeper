﻿using System.Linq;
using zookeeper.Data;
using zookeeper.Models;
using System.Collections.Generic;

namespace zookeeper.Validators
{
    public class AnimalValidator : IModelValidator<Animal>
    {
        private ZooKeeperContext _context;

        public AnimalValidator(ZooKeeperContext context)
        {
            _context = context;
        }

        public string[] Validate(Animal animal)
        {
            var messages = new List<string>();

            var zooUid = animal.ZooUid;

            if (zooUid == null)
            {
                return messages.ToArray();
            }

            var zoo = _context.Zoos.FirstOrDefault(z => z.Uid == zooUid);

            if (zoo == null)
            {
                messages.Add("Zoo is not found");
            }

            return messages.ToArray();
        }
    }
}
