﻿using System;
using System.Linq;
using zookeeper.Data;
using zookeeper.Models;
using System.Collections.Generic;

namespace zookeeper.Validators
{
    public class ZooValidator : IModelValidator<Zoo>
    {
        private ZooKeeperContext _context;

        public ZooValidator(ZooKeeperContext context)
        {
            _context = context;
        }

        public string[] Validate(Zoo model)
        {
            var errorMessages = new List<string>();

            if (model.Uid.HasValue && model.CageSizes != null)
            {
                var animals = _context.Animals.Where(a => a.ZooUid == model.Uid);

                if (animals.Any(a => model.CageSizes.Contains(a.Size) == false))
                {
                    errorMessages.Add("Cage sizes can not be changed, please check animals sizes");
                }
            }

            return errorMessages.ToArray();
        }
    }
}
