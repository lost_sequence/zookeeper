﻿namespace zookeeper.Validators
{
    public interface IModelValidator<TModel>
    {
        string[] Validate(TModel model);
    }
}
