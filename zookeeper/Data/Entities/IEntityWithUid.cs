﻿using System;

namespace zookeeper.Data.Entities
{
    public interface IEntityWithUid
    {
        Guid Uid { get; set; }
    }
}
