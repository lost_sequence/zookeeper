﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using zookeeper.Enums;

namespace zookeeper.Data.Entities
{
    public class DbAnimal : IEntityWithUid
    {
        [Required]
        public int Age { get; set; }

        [Key]
        public Guid Uid { get; set; }

        [Required]
        [MaxLength(100)]
        public string Type { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        public string Size { get; set; }

        // Zoo reference
        [ForeignKey("ZooUid")]
        public DbZoo Zoo { get; set; }

        public Guid? ZooUid { get; set; }
    }
}
