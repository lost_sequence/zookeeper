﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace zookeeper.Data.Entities
{
    public class DbZoo : IEntityWithUid
    {
        [Key]
        public Guid Uid { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        [MaxLength(128)]
        public string Email { get; set; }

        [Required]
        [MaxLength(20)]
        public string Phone { get; set; }

        [Required]
        [MaxLength(400)]
        public string Address { get; set; }

        public string CageSizes { get; set; }

        public ICollection<DbAnimal> Animals { get; set; }
    }
}
