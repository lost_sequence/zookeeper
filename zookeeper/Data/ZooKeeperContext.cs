﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zookeeper.Data.Entities;

namespace zookeeper.Data
{
    public class ZooKeeperContext : DbContext
    {
        public ZooKeeperContext(DbContextOptions<ZooKeeperContext> options) : base(options)
            { }

        public DbSet<DbZoo> Zoos { get; set; }
        public DbSet<DbAnimal> Animals { get; set; }

        internal object FirstOrDefault()
        {
            throw new NotImplementedException();
        }
    }
}
