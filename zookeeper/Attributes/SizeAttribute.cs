﻿using System;
using System.Drawing;
using System.ComponentModel.DataAnnotations;

namespace zookeeper.Attributes
{
    public class SizeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var size = (string)validationContext.ObjectInstance;

            if (Enum.TryParse(size, out Size _allowedSizes) == false)
            {
                new ValidationResult($"Animal size {size} is not supported.");
            }

            return ValidationResult.Success;
        }
    }
}
