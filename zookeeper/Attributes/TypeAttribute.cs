﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace zookeeper.Attributes
{
   
    public class TypeAttribute
    {
    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        var size = (string)validationContext.ObjectInstance;

        if (Enum.TryParse(size, out Size _allowedSizes) == false)
        {
            new ValidationResult($"Animal size {size} is not supported.");
        }

        return ValidationResult.Success;
    }

}
}
