﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace zookeeper.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class IsEnumDefinedAttribute : ValidationAttribute
    {
        private Type _enumType;

        public IsEnumDefinedAttribute(Type enumType)
        {
            _enumType = enumType;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool isValueDefined = true;
            object notSupportedValue = value;

            if (value == null)
            {
                return ValidationResult.Success;
            }

            if (value is ICollection)
            {
                var collection = value as ICollection;

                foreach (var element in collection)
                {
                    if (Enum.IsDefined(_enumType, element) == false)
                    {
                        isValueDefined = false;
                        notSupportedValue = element;

                        break;
                    }
                }
            }
            else
            {
                isValueDefined = Enum.IsDefined(_enumType, value);
            }
            
            return isValueDefined
                ? ValidationResult.Success
                : new ValidationResult($"{_enumType.Name} {notSupportedValue} is not supported");
        }
    }
}
