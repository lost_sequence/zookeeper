﻿using System;
using zookeeper.Enums;
using System.ComponentModel.DataAnnotations;

namespace zookeeper.Models
{
    public class AnimalSize
    {
        public Guid Uid { get; set; }

        [Required]
        [Attributes.Enum(typeof(Size))]
        public string Size { get; set; }
    }
}
