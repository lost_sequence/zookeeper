using System;
using zookeeper.Enums;
using zookeeper.Attributes;

namespace zookeeper.Models
{
	public class Animal : IModelWithUid
    {
        public virtual int? Age { get; set; }

        public virtual Guid? Uid { get; set; }

        [IsEnumDefined(typeof(Size))]
        public virtual string Size { get; set; }

        public virtual string Name { get; set; }

        [IsEnumDefined(typeof(AnimalType))]
        public virtual string Type { get; set; }

        public virtual Guid? ZooUid { get; set; }
    }
}