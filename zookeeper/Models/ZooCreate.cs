﻿using System.ComponentModel.DataAnnotations;
using zookeeper.Attributes;

namespace zookeeper.Models
{
    public class ZooCreate : Zoo
    {
        [Required]
        public override string Name { get; set; }

        [Required]
        public override string Email { get; set; }

        [Required]
        public override string Phone { get; set; }

        [Required]
        public override string Address { get; set; }
    }
}
