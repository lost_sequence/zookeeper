using System;
using zookeeper.Enums;
using zookeeper.Constants;
using zookeeper.Attributes;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace zookeeper.Models
{
    public class Zoo : IModelWithUid
    {
        public virtual Guid? Uid { get; set; }

        public virtual string Name { get; set; }

        [RegularExpression(RegexPattern.EmailPattern,
            ErrorMessage = "Invalid email format, please use user@domain.ru")]
        public virtual string Email { get; set; }

        [RegularExpression(RegexPattern.PhonePattern,
            ErrorMessage = "Invalid phone format, please use +11111")]
        public virtual string Phone { get; set; }

        public virtual string Address { get; set; }

        [IsEnumDefined(typeof(Size))]
        public virtual string[] CageSizes { get; set; }
    }
}