﻿using System.Net;

namespace zookeeper.Models
{
    public class Response
    {
        public object Data { get; set; }

        public string[] ErrorMessages { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }
    }
}
