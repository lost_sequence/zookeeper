﻿using System;

namespace zookeeper.Models
{
    public interface IModelWithUid
    {
        Guid? Uid { get; set; }
    }
}
