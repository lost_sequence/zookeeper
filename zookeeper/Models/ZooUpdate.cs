﻿using System;
using System.ComponentModel.DataAnnotations;

namespace zookeeper.Models
{
    public class ZooUpdate : Zoo
    {
        [Required]
        public override Guid? Uid { get; set; }
    }
}
