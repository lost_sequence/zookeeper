﻿using System.ComponentModel.DataAnnotations;
using zookeeper.Attributes;

namespace zookeeper.Models
{
    public class AnimalCreate : Animal
    {
        [Required]
        public override int? Age { get; set; }

        [Required]
        public override string Type { get; set; }

        [Required]
        public override string Size { get; set; }

        [Required]
        public override string Name { get; set; }
    }
}
