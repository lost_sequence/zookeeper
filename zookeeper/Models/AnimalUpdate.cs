﻿using System;
using System.ComponentModel.DataAnnotations;

namespace zookeeper.Models
{
    public class AnimalUpdate : Animal
    {
        [Required]
        public override Guid? Uid { get; set; }
    }
}
