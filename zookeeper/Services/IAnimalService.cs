﻿using System;
using zookeeper.Models;

namespace zookeeper.Services
{
    public interface IAnimalService
    {
        Response SetFree(Guid uid);

        Response GetByZoo(Guid uid);
    }
}