﻿using System;
using System.Net;
using System.Linq;
using zookeeper.Data;
using zookeeper.Models;
using zookeeper.Validators;
using zookeeper.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace zookeeper.Services
{
    public class AnimalService : EntityServiceBase<DbAnimal, Animal>, IAnimalService
    {
        public AnimalService(
            ZooKeeperContext context,
            IModelValidator<Animal> validator,
            IEntityMapper<DbAnimal, Animal> mapper) : base(context, validator, mapper)
        {
        }

        protected override DbSet<DbAnimal> DbSet => Context.Animals;

        public Response GetByZoo(Guid zooUid)
        {
            var animals = 
                DbSet
                .Where(a => a.ZooUid == zooUid)
                .ToArray()
                .Select(a => 
                {
                    var animal = new Animal();
                    Mapper.MapToModel(a, animal);

                    return animal;
                })
                .ToArray();

            return new Response
            {
                Data = animals,
                HttpStatusCode = HttpStatusCode.OK
            };
        }

        public Response SetFree(Guid uid)
        {
            var model = new Animal
            {
                Uid = uid,
                ZooUid = null
            };

            return Update(model);
        }
    }
}
