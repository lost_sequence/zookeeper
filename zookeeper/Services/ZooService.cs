﻿using zookeeper.Data;
using zookeeper.Models;
using zookeeper.Validators;
using zookeeper.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace zookeeper.Services
{
    public class ZooService : EntityServiceBase<DbZoo, Zoo>
    {
        public ZooService(
            ZooKeeperContext context,
            IModelValidator<Zoo> validator,
            IEntityMapper<DbZoo, Zoo> mapper)
            : base(context, validator, mapper)
        {
        }

        protected override DbSet<DbZoo> DbSet => Context.Zoos;
    }
}
