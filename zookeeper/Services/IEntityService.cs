﻿using System;
using zookeeper.Models;
using zookeeper.Data.Entities;

namespace zookeeper.Services
{
    public interface IEntityService<TEntity, TModel>
        where TModel : class, IModelWithUid
        where TEntity : class, IEntityWithUid
    {
        Response Get();

        Response Get(Guid uid);

        Response Delete(Guid uid);

        Response Create(TModel model);

        Response Update(TModel model);
    }
}
