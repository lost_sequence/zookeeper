﻿using System;
using Serilog;
using System.Linq;
using System.Net;
using zookeeper.Data;
using zookeeper.Models;
using zookeeper.Validators;
using zookeeper.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Serilog.Sinks.SystemConsole.Themes;

namespace zookeeper.Services
{
    public abstract class EntityServiceBase<TEntity, TModel> : IEntityService<TEntity, TModel>
        where TModel : class, IModelWithUid, new()
        where TEntity : class, IEntityWithUid, new()
    {
        protected abstract DbSet<TEntity> DbSet { get; }

        protected ZooKeeperContext Context { get; private set; }
        protected IEntityMapper<TEntity, TModel> Mapper { get; set; }

        private ILogger _logger;
        private IModelValidator<TModel> _validator;

        public EntityServiceBase(
            ZooKeeperContext context,
            IModelValidator<TModel> validator,
            IEntityMapper<TEntity, TModel> mapper)
        {
            Mapper = mapper;
            Context = context;

            _logger = GetLogger();
            _validator = validator;
        }

        public Response Get()
        {
            var zoos = DbSet
                .Select(e => e)
                .ToArray();

            return new Response
            {
                Data = zoos.Select(z => GetInternal(z)),
                HttpStatusCode = HttpStatusCode.OK
            };
        }

        public Response Get(Guid uid)
        {
            var existingEntity = DbSet.FirstOrDefault(e => e.Uid == uid);

            return new Response
            {
                Data = GetInternal(existingEntity),
                HttpStatusCode = HttpStatusCode.OK
            };
        }

        private TModel GetInternal(TEntity entity)
        {
            var model = new TModel();
            Mapper.MapToModel(entity, model);

            return model;
        }

        public Response Create(TModel model)
        {
            Response response = new Response();

            if (model == null)
            {
                var errorMessage = $"Bad argument {typeof(TModel).Name}, please check object fields";
                response.ErrorMessages = new[] { errorMessage };

                _logger.Warning(errorMessage);
            }

            var errorMessages = _validator.Validate(model);

            if (errorMessages != null && errorMessages.Any())
            {
                response.ErrorMessages = errorMessages;
                response.HttpStatusCode = HttpStatusCode.BadRequest;

                return response;
            }

            try
            {
                var newEntity = new TEntity();
                Mapper.MapFromModel(model, newEntity);

                DbSet.Add(newEntity);
                Context.SaveChanges();

                response.Data = newEntity.Uid;
                response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                response.ErrorMessages = new[]
                    {
                        $"Error occured while creating {typeof(TModel).Name}. Please contact with the support."
                    };

                _logger.Error(e.InnerException != null ? e.InnerException.Message : e.Message);

                response.HttpStatusCode = HttpStatusCode.InternalServerError;
            }

            return response;
        }

        public virtual Response Update(TModel model)
        {
            var errorMessages = _validator.Validate(model);
            var response = new Response();

            if (errorMessages != null && errorMessages.Any())
            {
                response.ErrorMessages = errorMessages;
                response.HttpStatusCode = HttpStatusCode.BadRequest;

                return response;
            }

            var existingEntity = DbSet.FirstOrDefault(a => a.Uid == model.Uid);

            if (existingEntity != null)
            {
                try
                {
                    Mapper.MapFromModel(model, existingEntity);

                    DbSet.Update(existingEntity);
                    Context.SaveChanges();

                    response.HttpStatusCode = HttpStatusCode.OK;
                }
                catch (Exception e)
                {
                    response.ErrorMessages = new[]
                    {
                        $"Error occured while updating {typeof(TModel).Name}. Please contact with the support."
                    };

                    _logger.Error(e.InnerException != null ? e.InnerException.Message : e.Message);

                    response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                response.HttpStatusCode = HttpStatusCode.BadRequest;

                var errorMessage = $"{typeof(TModel).Name} not found";
                response.ErrorMessages = new[] { errorMessage };

                _logger.Warning(errorMessage);
            }

            return response;
        }

        public Response Delete(Guid uid)
        {
            var existingAnimal = DbSet.FirstOrDefault(a => a.Uid == uid);
            var response = new Response();

            if (existingAnimal != null)
            {
                try
                {
                    DbSet.Remove(existingAnimal);
                    Context.SaveChanges();

                    response.HttpStatusCode = HttpStatusCode.OK;
                }
                catch (Exception e)
                {
                    response.ErrorMessages = new[]
                    {
                        $"Error occured while deleting {typeof(TModel).Name}." +
                        $"Check if zoo has animals or contact with the support."
                    };

                    _logger.Error(e.InnerException != null ? e.InnerException.Message : e.Message);

                    response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                response.HttpStatusCode = HttpStatusCode.BadRequest;

                var errorMessage = $"{typeof(TModel).Name} not found";
                response.ErrorMessages = new[] { errorMessage };

                _logger.Warning(errorMessage);
            }

            return response;
        }

        private ILogger GetLogger()
        {
            return new LoggerConfiguration()
                        .MinimumLevel.Debug()
                        .Enrich.FromLogContext()
                        .WriteTo.File(@"log/zookeeper_log.txt")
                        .WriteTo
                        .Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Literate)
                        .CreateLogger();
        }
    }
}
