﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace zookeeper.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Zoos",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(maxLength: 400, nullable: true),
                    CageSize = table.Column<byte>(nullable: false),
                    Email = table.Column<string>(maxLength: 128, nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zoos", x => x.Uid);
                });

            migrationBuilder.CreateTable(
                name: "Animals",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Age = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: true),
                    Size = table.Column<byte>(nullable: false),
                    Type = table.Column<string>(maxLength: 100, nullable: true),
                    ZooUid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Animals", x => x.Uid);
                    table.ForeignKey(
                        name: "FK_Animals_Zoos_ZooUid",
                        column: x => x.ZooUid,
                        principalTable: "Zoos",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Animals_ZooUid",
                table: "Animals",
                column: "ZooUid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Animals");

            migrationBuilder.DropTable(
                name: "Zoos");
        }
    }
}
