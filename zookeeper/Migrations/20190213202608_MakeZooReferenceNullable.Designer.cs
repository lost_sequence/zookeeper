﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using zookeeper.Data;
using zookeeper.Enums;

namespace zookeeper.Migrations
{
    [DbContext(typeof(ZooKeeperContext))]
    [Migration("20190213202608_MakeZooReferenceNullable")]
    partial class MakeZooReferenceNullable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("zookeeper.Data.Entities.DbAnimal", b =>
                {
                    b.Property<Guid>("Uid")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Age");

                    b.Property<string>("Name")
                        .HasMaxLength(200);

                    b.Property<int>("Size");

                    b.Property<int>("Type")
                        .HasMaxLength(100);

                    b.Property<Guid?>("ZooUid");

                    b.HasKey("Uid");

                    b.HasIndex("ZooUid");

                    b.ToTable("Animals");
                });

            modelBuilder.Entity("zookeeper.Data.Entities.DbZoo", b =>
                {
                    b.Property<Guid>("Uid")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(400);

                    b.Property<byte>("CageSize");

                    b.Property<string>("Email")
                        .HasMaxLength(128);

                    b.Property<string>("Name")
                        .HasMaxLength(200);

                    b.Property<string>("Phone")
                        .HasMaxLength(20);

                    b.HasKey("Uid");

                    b.ToTable("Zoos");
                });

            modelBuilder.Entity("zookeeper.Data.Entities.DbAnimal", b =>
                {
                    b.HasOne("zookeeper.Data.Entities.DbZoo", "Zoo")
                        .WithMany("Animals")
                        .HasForeignKey("ZooUid");
                });
#pragma warning restore 612, 618
        }
    }
}
