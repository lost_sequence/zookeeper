﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace zookeeper.Migrations
{
    public partial class AlterZooMakeCageSizesString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CageSize",
                table: "Zoos");

            migrationBuilder.AddColumn<string>(
                name: "CageSizes",
                table: "Zoos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CageSizes",
                table: "Zoos");

            migrationBuilder.AddColumn<byte>(
                name: "CageSize",
                table: "Zoos",
                nullable: false,
                defaultValue: (byte)0);
        }
    }
}
