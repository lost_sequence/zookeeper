﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace zookeeper.Migrations
{
    public partial class MakeZooReferenceNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Animals_Zoos_ZooUid",
                table: "Animals");

            migrationBuilder.AlterColumn<Guid>(
                name: "ZooUid",
                table: "Animals",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<int>(
                name: "Type",
                table: "Animals",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Size",
                table: "Animals",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AddForeignKey(
                name: "FK_Animals_Zoos_ZooUid",
                table: "Animals",
                column: "ZooUid",
                principalTable: "Zoos",
                principalColumn: "Uid",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Animals_Zoos_ZooUid",
                table: "Animals");

            migrationBuilder.AlterColumn<Guid>(
                name: "ZooUid",
                table: "Animals",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "Animals",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(int),
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<byte>(
                name: "Size",
                table: "Animals",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Animals_Zoos_ZooUid",
                table: "Animals",
                column: "ZooUid",
                principalTable: "Zoos",
                principalColumn: "Uid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
