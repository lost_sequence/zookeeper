using System;
using zookeeper.Data;
using zookeeper.Models;
using zookeeper.Services;
using zookeeper.Extensions;
using zookeeper.Data.Entities;
using Microsoft.AspNetCore.Mvc;

namespace zookeeper.Controllers
{
	public class AnimalController : Controller
	{
        private ZooKeeperContext _context;
        private IEntityService<DbAnimal, Animal> _animalService;

        public AnimalController(IEntityService<DbAnimal, Animal> animalService, ZooKeeperContext context) {
            _context = context;
            _animalService = animalService;
        }

        [HttpGet]
        public IActionResult List()
        {
            return this.ResolveResult(_animalService.Get());
        }

        [HttpGet]
		public IActionResult Get(Guid uid)
		{
            return this.ResolveResult(_animalService.Get(uid));
        }

        [HttpPost]
        public IActionResult Delete([FromBody] AnimalUpdate animal)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            return this.ResolveResult(_animalService.Delete(animal.Uid.Value));
        }

        [HttpPost]
        public IActionResult Create([FromBody] AnimalCreate animal)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            return this.ResolveResult(_animalService.Create(animal));
        }

        [HttpPost]
        public IActionResult Update([FromBody] AnimalUpdate animal)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            return this.ResolveResult(_animalService.Update(animal));
        }

        [HttpPost]
        public IActionResult SetFree([FromBody] AnimalUpdate animal)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var extendedService = (IAnimalService)_animalService;

            return this.ResolveResult(extendedService.SetFree(animal.Uid.Value));
        }

        [HttpGet]
        public IActionResult GetByZoo(Guid uid)
        {
            var extendedService = (IAnimalService)_animalService;

            return this.ResolveResult(extendedService.GetByZoo(uid));
        }
    }
}