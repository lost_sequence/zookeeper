using System;
using zookeeper.Data;
using zookeeper.Models;
using zookeeper.Services;
using zookeeper.Extensions;
using zookeeper.Data.Entities;
using Microsoft.AspNetCore.Mvc;

namespace zookeeper.Controllers
{
	public class ZooController : Controller
    {
        private ZooKeeperContext _context;
        private IEntityService<DbZoo, Zoo> _zooService;

        public ZooController(IEntityService<DbZoo, Zoo> zooService, ZooKeeperContext context)
        {
            _context = context;
            _zooService = zooService;
        }

        [HttpGet]
        public IActionResult List()
        {
            return this.ResolveResult(_zooService.Get());
        }

        [HttpGet]
        public IActionResult Get(Guid uid)
        {
            return this.ResolveResult(_zooService.Get(uid));
        }

        [HttpPost]
        public IActionResult Delete([FromBody] ZooUpdate zoo)
        {
            return this.ResolveResult(_zooService.Delete(zoo.Uid.Value));
        }

        [HttpPost]
        public IActionResult Create([FromBody] ZooCreate zoo)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            return this.ResolveResult(_zooService.Create(zoo));
        }

        [HttpPost]
        public IActionResult Update([FromBody] ZooUpdate zoo)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            return this.ResolveResult(_zooService.Update(zoo));
        }
    }
}