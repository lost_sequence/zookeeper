﻿using System;

namespace zookeeper.Enums
{
    public enum Size
    {
        Big,
        Small,
        Medium
    }
}
