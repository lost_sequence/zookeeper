namespace zookeeper.Enums
{
	public enum AnimalType
	{
        Bird,
        Mammal,
        Reptile,
        Dinosaur
    }
}