﻿using zookeeper.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace zookeeper.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseZooKeeper(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<ZooKeeperContext>();
                context.Database.Migrate();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "ZooKeeper",
                    template: "api/{controller}/{action}",
                    defaults: new { controller = "Animals", action = "Get" });
            });
        }
    }
}
