﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using zookeeper.Data;
using zookeeper.Data.Entities;
using zookeeper.Mappers;
using zookeeper.Models;
using zookeeper.Services;
using zookeeper.Validators;

namespace zookeeper.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddZooKeeper(this IServiceCollection services, string connectionString)
        {
            // services
            services.AddTransient<IEntityService<DbZoo, Zoo>, ZooService>();
            services.AddTransient<IEntityService<DbAnimal, Animal>, AnimalService>();

            // validators
            services.AddTransient<IModelValidator<Zoo>, ZooValidator>();
            services.AddTransient<IModelValidator<Animal>, AnimalValidator>();

            // mappers
            services.AddTransient<IEntityMapper<DbZoo, Zoo>, ZooMapper>();
            services.AddTransient<IEntityMapper<DbAnimal, Animal>, AnimalMapper>();

            // database
            services.AddDbContext<ZooKeeperContext>(options =>
                options.UseSqlServer(connectionString));
        }
    }
}
