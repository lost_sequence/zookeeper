﻿using System.Net;
using zookeeper.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace zookeeper.Extensions
{
    public static class ControllerExtensions
    {
        public static IActionResult ResolveResult(this Controller controller, Response response)
        {
            if (response.HttpStatusCode == HttpStatusCode.BadRequest)
            {
                return controller.BadRequest(response.ErrorMessages);
            }

            if (response.HttpStatusCode == HttpStatusCode.InternalServerError)
            {
                return controller.StatusCode((int)HttpStatusCode.InternalServerError, response.ErrorMessages);
            }

            if (response.Data == null)
            {
                return controller.Ok();
            }
            else
            {
                return controller.Ok(response.Data);
            }
        }
    }
}
