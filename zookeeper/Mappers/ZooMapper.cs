﻿using System;
using Newtonsoft.Json;
using zookeeper.Models;
using zookeeper.Data.Entities;

namespace zookeeper.Mappers
{
    public class ZooMapper : IEntityMapper<DbZoo, Zoo>
    {
        public void MapToModel(DbZoo entity, Zoo model)
        {
            model.Uid = entity.Uid;
            model.Name = entity.Name;
            model.Phone = entity.Phone;
            model.Email = entity.Email;
            model.Address = entity.Address;
            model.CageSizes = entity.CageSizes == null 
                ? null
                : JsonConvert.DeserializeObject<string[]>(entity.CageSizes);
        }

        public void MapFromModel(Zoo model, DbZoo entity)
        {
            if (model.Name != null)
            {
                entity.Name = model.Name;
            }

            if (model.Phone != null)
            {
                entity.Phone = model.Phone;
            }

            if (model.Email != null)
            {
                entity.Email = model.Email;
            }

            if (model.Address != null)
            {
                entity.Address = model.Address;
            }

            entity.Uid = model.Uid ?? Guid.NewGuid();
            entity.CageSizes = model.CageSizes == null
                ? null
                : JsonConvert.SerializeObject(model.CageSizes);
        }
    }
}
