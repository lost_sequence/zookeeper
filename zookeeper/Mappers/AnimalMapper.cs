﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using zookeeper.Data;
using zookeeper.Data.Entities;
using zookeeper.Models;

namespace zookeeper.Mappers
{
    public class AnimalMapper : IEntityMapper<DbAnimal, Animal>
    {
        ZooKeeperContext _context;

        public AnimalMapper(ZooKeeperContext context)
        {
            _context = context;
        }

        public void MapToModel(DbAnimal entity, Animal model)
        {
            model.Uid = entity.Uid;
            model.Age = entity.Age;
            model.Name = entity.Name;
            model.Size = entity.Size;
            model.Type = entity.Type;
            model.ZooUid = entity.ZooUid; 
        }

        public void MapFromModel(Animal model, DbAnimal entity)
        {
            if (model.Age.HasValue)
            {
                entity.Age = model.Age.Value;
            }

            if (model.Name != null)
            {
                entity.Name = model.Name;
            }

            if (model.Size != null)
            {
                entity.Size = model.Size;
            }

            if (model.Type != null)
            {
                entity.Type = model.Type;
            }
            
            entity.Uid = model.Uid ?? Guid.NewGuid();

            MapZooFromModel(model, entity);
        }

        private void MapZooFromModel(Animal model, DbAnimal entity)
        {
            if (model.ZooUid.HasValue == false)
            {
                entity.ZooUid = model.ZooUid;
                return;
            }

            var size = model.Size ?? entity.Size;
            var zoo = _context.Zoos.FirstOrDefault(z => z.Uid == model.ZooUid);

            // if zooUid has value, we must check animal size,
            // if animal size is not supported by the zoo, we detach animal from zoo
            if (zoo.CageSizes.Contains(size))
            {
                entity.ZooUid = model.ZooUid;
            }
            else
            {
                entity.ZooUid = null;
            }
        }
    }
}
