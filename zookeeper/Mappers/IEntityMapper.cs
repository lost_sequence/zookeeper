﻿namespace zookeeper.Models
{
    public interface IEntityMapper<TEntity, TModel> where TModel : class
    {
        void MapToModel(TEntity entity, TModel model);

        void MapFromModel(TModel model, TEntity entity);
    }
}
